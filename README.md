# Overview
Very simple Python UI based on [Tkinter](https://docs.python.org/2/library/tkinter.html) 
It can be used to: 
- run pytlint on a folder 
- browse result with filters and search capability

To be used as a dummy project for Tkinter or.. why not for Pylint!

# Requirement
You need to have installed: 
- pylint

# Usage
```sh
python pylintui.py
```
![ui](pylintui.png "UI")