import Tkinter
import tkFileDialog
import os

from subprocess import Popen,PIPE,STDOUT,call

class simpleapp_tk(Tkinter.Tk):
    def __init__(self,parent):
        Tkinter.Tk.__init__(self,parent)
        self.parent = parent
        self.initialize()
        self.last_output_content = str()

    def ask_directory(self):
        dirname = tkFileDialog.askdirectory()
        if dirname:
            self.entryVariable.set(dirname)

    def initialize(self):
        self.grid()

        
        path_label = Tkinter.Label(self, text="Path")
        path_label.grid(column=0,row=0,sticky='EW')
        self.entryVariable = Tkinter.StringVar()
        self.entry = Tkinter.Entry(self,textvariable=self.entryVariable)
        self.entry.grid(column=1,row=0,sticky='EW')
        self.entry.bind("<Return>", self.ask_directory)

        self.button_open = Tkinter.Button(self,text=u"Open",
                                command=self.ask_directory)
        self.button_open.grid(column=2,row=0,sticky='EW')

        self.button_run = Tkinter.Button(self,text=u"Run",
                                command=self.run_pylint)
        self.button_run.grid(column=3,row=0,sticky='EW')
        
        #################

        enable_label = Tkinter.Label(self, text="Enable")
        enable_label.grid(column=0,row=1,sticky='EW')
        self.enable_variable = Tkinter.StringVar()
        self.entry_enable = Tkinter.Entry(self,textvariable=self.enable_variable)
        self.entry_enable.grid(column=1,row=1,sticky='EW')
        self.enable_variable.set(u"")

        self.button_filter_enable = Tkinter.Button(self,text=u"Filter",
                                command=self.filter_enabled_output)
        self.button_filter_enable.grid(column=2,row=1, columnspan=2,sticky='EW')
    
        #################

        disable_label = Tkinter.Label(self, text="Disable")
        disable_label.grid(column=0,row=2,sticky='EW')
        self.disable_variable = Tkinter.StringVar()
        self.entry_disable = Tkinter.Entry(self,textvariable=self.disable_variable)
        self.entry_disable.grid(column=1,row=2,sticky='EW')
        self.disable_variable.set(u"")

        self.button_filter_disable = Tkinter.Button(self,text=u"Filter",
                                command=self.filter_disabled_output)
        self.button_filter_disable.grid(column=2,row=2, columnspan=2,sticky='EW')

        #################

        self.output = Tkinter.Text(self)
        self.output.grid(column=0,row=3, columnspan=4, sticky='NSEW')
        self.yscrollbar = Tkinter.Scrollbar(self) # height= not permitted here!
        self.xscrollbar = Tkinter.Scrollbar(self) # height= not permitted here!

        self.output.config(yscrollcommand= self.yscrollbar.set)
        self.output.config(xscrollcommand= self.xscrollbar.set)

        self.yscrollbar.config(command= self.output.yview)
        self.xscrollbar.config(command= self.output.xview)

        #################

        #################

        self.status_variable = Tkinter.StringVar()
        self.entry_status = Tkinter.Entry(self,textvariable=self.status_variable)
        self.entry_status.grid(column=0,row=4,sticky='NSEW', columnspan=2)
        self.status_variable.set(u"")


        self.filter_result_variable = Tkinter.StringVar()
        self.entry_filter_result = Tkinter.Entry(self,textvariable=self.filter_result_variable)
        self.entry_filter_result.grid(column=2,row=4,sticky='NSEW', columnspan=2)
        self.filter_result_variable.set(u"")


        #################

        self.grid_columnconfigure(0,weight=3)
        self.grid_columnconfigure(1,weight=85)
        self.grid_columnconfigure(2,weight=6)
        self.grid_columnconfigure(3,weight=6)

        self.grid_rowconfigure(3,weight=1)
        self.resizable(True,True)
        self.update()
        self.geometry(self.geometry()) 

    def polulate_output(self, text):
        #self.output.config(state="normal")
        self.output.delete('1.0', Tkinter.END)
        self.output.insert(Tkinter.END, text)

        self.filter_result_variable.set("Found: {0}".format(text.count(': [')))

        #self.output.config(state="disabled")  


    def build_pylint_cmd(self):
        cmd = "pylint --output-format=parseable --reports=yes"
        disables = self.disable_variable.get()
        if disables:
            cmd = cmd +' --disable={0}'.format(disables)
        enables = self.enable_variable.get()
        if enables:
            if not disables:
                cmd = cmd +' --disable=all'
            cmd = cmd +' --enable={0}'.format(enables) 
        return cmd+' '+self.entryVariable.get()

    def run_pylint(self):
        self.button_open.config(state="disabled")
        self.button_run.config(state="disabled")

        cmd = self.build_pylint_cmd()
        self.status_variable.set(cmd)
        proc=Popen(cmd, shell=True, stdout=PIPE, )
        output=proc.communicate()[0]
        
        self.polulate_output(output)
        self.last_output_content = output

        self.button_open.config(state="normal")
        self.button_run.config(state="normal")

        #self.canvas.create_text(200, 200, text=output)

    def filter_enabled_output(self):
        patterns = self.enable_variable.get()
        self.filter_output(patterns)

    def filter_disabled_output(self):
        patterns = self.disable_variable.get()
        self.filter_output(patterns)



    def filter_output(self, patterns):
        filtered_ouput = list()
        if not patterns:
            self.polulate_output(self.last_output_content)
        else: 
            splitted_patterns = patterns.split(',')

            for l in self.last_output_content.splitlines():
                for pattern in splitted_patterns:
                    if pattern in l:
                        filtered_ouput.append(l+os.linesep)
            self.polulate_output(''.join(filtered_ouput))

        

    """
    def OnButtonClick(self):
        self.pathVariable.set( self.entryVariable.get()+" (You clicked the button)" )
        self.entry.focus_set()
        self.entry.selection_range(0, Tkinter.END)

    def OnPressEnter(self,event):
        self.pathVariable.set( self.entryVariable.get()+" (You pressed ENTER)" )
        self.entry.focus_set()
        self.entry.selection_range(0, Tkinter.END)
    """

if __name__ == "__main__":
    app = simpleapp_tk(None)
    app.title('Pylint UI')
    app.mainloop()